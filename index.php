<?php

	include_once "_common/header.php";

	$file = '_inc/storage';
	mk_file( $file );

	$data = file_get_contents( $file );
	$data = json_decode( $data ) ?: [];


	//add new thing if form was submitted and can edit
	if ( can_edit() && ! empty($_POST) && ltrim($_POST['message']) ) {
		

		$post = (object)[
			'text' => ltrim($_POST['message']),
			'time' => time()
		];

		array_push($data, $post);
		file_put_contents( $file, json_encode($data) );

		
	};
	
	if ( can_edit() ) {
		include_once('_common/add-form.php');
	};

	$arr1 = ['one', 'two', 'three'];

	echo '<pre>';
		push_to_array($arr1, 5, 'five', true);
	echo '</pre>';
?>
	

	<section class="article-list">
		<?php foreach ( $data as $i => $item ) : $row = ++$i ?>

			<article class="<?= get_parity( $row )?>">
				<time datetime="<?= date( 'Y-m-d', $item->time) ?>">
					<?= date( 'j M Y', $item->time) ?>
				</time>
				<p><?= nl2br($item->text) ?></p>
			</article>

		<?php endforeach ?>
	</section>
	
<?php include_once "_common/footer.php" ?>