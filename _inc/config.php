<?php

// show all errors
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);


// require stuff
require_once('vendor/autoload.php');


// global variables
$base_url = 'http://localhost/webrebel2/JSON,%20XSS,%20COOKIES,%20SESSIONS/main/';


// global functions
require_once('functions.php');